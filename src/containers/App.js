import React from 'react';
import './App.css';
import Students from '../components/students/students';
import Hooks from '../components/hooks/fooks';

import ThemeContext from '../components/context/theme-context';

function App() {
  const title = 'Top Students list'
  return (
    <div className="App">
      <Hooks />
      <ThemeContext.Provider value='dark'>
        <Students title={title} />
      </ThemeContext.Provider>
    </div>
  );
}

export default App;

// import React, { Component} from 'react';
// import './App.css';
// import Students from '../components/students/students';


// class App extends Component() {

//   constructor(props) {
//     super(props); 
//     this.inputElementRef = React.createRef(); // will take ref of any jsx
//   }

//   state = {
//     title: 'Top Students list'
//   }

//   componentDidMount() {
//     console.log('[App.js]---componentDidMount');  
//     // this.inputElementRef.focus();  old way before 16.3
//     this.inputElementRef.current.focus();
//   }

//   render() {
//     console.log('[App.js]---render');
//     return (
//       <div className="App">
//         <Students title={this.state.title} />
//         <input type="text"  
//             // refs={(inputEl) => {this.inputElementRef = inputEl}}  // old way by callback ref
//             refs={this.inputElementRef}
//             placeholder="Enter your feedback" 
//         /> 
//       </div>
//     );
//   }
// }

// export default App;
