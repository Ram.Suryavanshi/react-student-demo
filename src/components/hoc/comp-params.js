import React from 'react';

const compParam = (WrappedComponent, style) => {
    return (props) => (
        <div style={style}>
            <WrappedComponent {...props}/>
        </div>
    )
}

export default compParam;