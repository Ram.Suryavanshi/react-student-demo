import React from 'react';

const withClass = props => {
    return(
        <div style={props.style}>
            {props.children}
        </div>
    );
};
export default withClass;
