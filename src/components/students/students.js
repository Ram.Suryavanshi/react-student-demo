import React, { Component } from 'react';
import Student from './student/student';
import WithStyle from '../hoc/with-style';
import CompParam from '../hoc/comp-params';
import Aux from '../hoc/aux';

class Students extends Component {

    constructor(props) {
        super(props);
        console.log('Students---[constructor]');
        this.inputElementRef = React.createRef();
    }

    state = {
        students: [
            { id: 1, name: 'Ram', age: 21, education: 'B.Tech' },
            { id: 2, name: 'Shayam', age: 18, education: '12th' },
            { id: 3, name: 'Balram', age: 30, education: 'M.Tech' }
        ],
        collegeName: 'VIT Pune',
        showData: false
    }   

    static getDerivedStateFromProps(props, state) {
        console.log('Students---[getDerivedStateFromProps]');       
        return state;
    }

    componentDidMount() {
        console.log('Students---[componentDidMount]');
        // this.inputElementRef.focus(); before react 16.3 > old one
        this.inputElementRef.current.focus();  // new in >16.3
    } 

    loadDataHandler = () => {

        this.setState({
            students: [
                { id: 1, name: 'Ram', age: 21, education: 'B.Tech' },
                { id: 2, name: 'Shayam', age: 18, education: '12th' },
                { id: 3, name: 'Balram', age: 30, education: 'M.Tech' }
            ],
            collegeName: 'VIT Pune',
            showData: true
        });
    }

    deleteHandler = (id) => {
        const index = this.state.students.findIndex(stud => stud.id === id);
        const students = [...this.state.students];
        students.splice(index, 1);
        this.setState({students});

    };

    nameChangeHandler = (event, id) => {
        const student = {...this.state.students[id]};
        student.name = event.target.value;
        const students = [ ...this.state.students ];
        students[id] = student;
        this.setState({students});
    }
 
    render() {
        console.log('Students---[render]');
        
        const student = [];
        if (this.state.showData) {
            this.state.students.map((stud, key) => {
                student.push(
                    <Student
                        key={stud.id + key}
                        name={stud.name}
                        age={stud.age}
                        education={stud.education}
                        collegeName={this.state.collegeName}
                        click={() => this.deleteHandler(stud.id)}
                        change={(event) => this.nameChangeHandler(event, key)}
                    />
                )
            });
        }

        return (
            <Aux>
                <h1>{this.props.title}</h1>
                <button onClick={this.loadDataHandler}>Load Students</button>
                {student}
                <input type="text"  
                    // ref={(inputEl) => {this.inputElementRef = inputEl}}  // old one
                    ref={this.inputElementRef}
                    placeholder="Enter your feedback" 
                /> 
                {/* We can focus element by atutoFocus propert too */}
            </Aux>
            // <WithStyle style={{display: 'flex',flexDirection: 'column', alignItems: 'center'}}>
            //   <h1>{this.props.title}</h1>
            //   <button onClick={this.loadDataHandler}>Load Students</button>
            //   {student}
            // </WithStyle>
        )
    }
}

const style = {display: 'flex', flexDirection: 'column', alignItems: 'center'}

export default CompParam(Students, style); // HOC with arguments
