import React, { Component } from 'react';
import WithStyle from '../../hoc/with-style';
import { render } from '@testing-library/react';
import themeContext from '../../context/theme-context';

class ContextTheme extends Component {

    static contextType = themeContext;

    
    render() {

        return(
            <p>
                {this.context}
            </p>
        )
    }
}

const student = ({name, age, education, collegeName, click, change, context}) => {

    console.log('Child student ---- rendering');

    const cardStyle = {
        width: '60%',
        padding: '2%',
        margin: '1%',
        borderRadius: '4px',
        background: '#f5f2f2',
        boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)'
    }

    return (
        <WithStyle style={cardStyle}>
            <p>Name: {name}</p>
            <p>Age: {age}</p>
            <p>Education: {education}</p>
            <p>College: {collegeName}</p>
            <p>Props Context: </p>
            <button onClick={click} >Delete</button>
            <input onChange={change} type="text" value={name} />
            <ContextTheme />
        </WithStyle>
    );
}

export default student;
