import React, { useState, useEffect, useRef } from 'react';

const Hooks = () => {

    const [ state, setState ] = useState({
       message: 'Welcome to hooks' 
    });

    const inputElementRef = useRef(null);

    // It called only initial time, block after that.
    useEffect(() => {
        console.log('[Hooks.js]------useEffects');
        inputElementRef.current.focus();
    }, []);

    useEffect(() => {
        console.log('[Hooks.js]------useEffects');
        // but will call evry update, if we want to call for specific data
        // In this case we can pass that data/object to useEffect function as arguments

        return () => {
            console.log('[Hooks.js]------cleanup work should do here');
        };

    }, []);

    return(
        <div>
            <p>{state.message}</p>
            <input type="text" ref={inputElementRef} placeholder="Hooks input data with useRef focus"/>
        </div>
    );
}

export default React.memo(Hooks);
